//
//  ViewController.m
//  XLsn0wVersionManager
//
//  Created by XLsn0w on 2016/12/29.
//  Copyright © 2016年 XLsn0w. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)checkVersion:(id)sender {
    //检查更新
    [XLsn0wVersionManager xlsn0w_updateVersionWithAppStoreID:@"1014895889" showInCurrentController:self isShowReleaseNotes:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
